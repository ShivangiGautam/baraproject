import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule, Response, Headers, RequestOptions } from '@angular/http';
import {RouterModule, Routes} from '@angular/router'
import { AppComponent } from './app.component';
import { MyserviceService } from './myservice.service';
import { GetAccountDataComponent } from './get-account-data/get-account-data.component';

const appRoutes: Routes = [
  { path: 'getacc', component: GetAccountDataComponent },
];
@NgModule({
  declarations: [
    AppComponent,
    GetAccountDataComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    HttpModule
  ],
  providers: [MyserviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
