import { Component } from '@angular/core';
import { MyserviceService } from './myservice.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private url = 'http://www.baraproject.nl/api/Account';
  public userData: any = {};
  constructor(
    public myService: MyserviceService
  ){}

  ngOnInit(){
    this.myService.getData(this.url).subscribe(res=>{
      if(res){
        this.userData = res;
      }
    });
  }
}