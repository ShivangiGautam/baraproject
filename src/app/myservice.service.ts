import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

@Injectable()
export class MyserviceService {
    constructor(private _http: Http) { }
    getData(dataUrl: string, params?: Object) {
        var Url = dataUrl;
        let headers = new Headers();
        headers.append('Accept', 'application/json')
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Basic amFuZUBiYXJhLm5sOndlbGtvbTEyMw==');
        var options = new RequestOptions({ headers: headers });
        return this._http.get(Url, options)
            .map((res: Response) => res.json())
            .do((data) =>{})
            .catch(this.handleError);

    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error || 'Server error');

    }
    }