import { Component, OnInit } from '@angular/core';
import { MyserviceService } from '../myservice.service';

@Component({
  selector: 'app-get-account-data',
  templateUrl: './get-account-data.component.html',
  styleUrls: ['./get-account-data.component.css']
})
export class GetAccountDataComponent implements OnInit {
  constructor(public myService: MyserviceService) {
    
  }

  ngOnInit() {
  }

}
